const fs = require('fs');
class Dictionary {
  constructor() {
    this.store = {}
    this.storeFile = 'store.json'
    this.list = this.list.bind(this);
    this.add = this.add.bind(this);
    this.get = this.get.bind(this);
    this.remove = this.remove.bind(this);
    this.clear = this.clear.bind(this);
    this.save = this.save.bind(this);
    this.store = (JSON.parse(fs.readFileSync(this.storeFile)))
  }

  add(args) {
    // if key already exist notify
    if (args[0] in this.store) {
      console.log("this key already exists");
      return
    }
    this.store[args[0]] = args[1];
    this.save(this.store);
    console.log(`new entery ${args[0]} : ${args[1]} was added`)
  }

  list() {
    // if empty notify
    if (Object.keys(this.store).length === 0) {
      console.log("dictionary is empty, add values by calling add key value")
      return
    }
    Object.keys(this.store).map((key, i) => {
      let item = `${i + 1}) ${key} : ${this.store[key]}`
      console.log(item);
    });
  }

  get(args) {
    // if key not found notify
    if (!(args[0] in this.store)) {
      console.log("key not found");
      return
    }
    let value = this.store[args[0]]
    console.log(value)
  }

  remove(args) {
    // if key not found notify
    if (!(args[0] in this.store)) {
      console.log("key not found");
      return
    }
    delete this.store[args[0]];
    this.save(this.store);
    console.log(`item ${args[0]} was deleted`)
  }

  clear() {
    const length = Object.keys(this.store).length;
    this.store = {};
    this.save(this.store);
    console.log(`${length} enteries were deleted, dictionary is now blank`)
  }

  save(obj) {
    fs.writeFile(this.storeFile, JSON.stringify(obj), (err) => {
      if (err) throw err;
    });
  }
}

module.exports = Dictionary;