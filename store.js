#!/usr/bin/env node
const Dictionary = require('./Dictionary')
const argv = process.argv.slice(2);
const dict = new Dictionary();

selectAction = (action, args) => {
  const actions = {
    'list': dict.list,
    'add': dict.add,
    'get': dict.get,
    'remove': dict.remove,
    'clear': dict.clear,
  }
  return actions[action](args);
}

if (argv.length > 0) {
  selectAction(argv[0], argv.slice(1));
}
if (argv.length === 0) {
  console.log("Welcome to Store \nyou can call list, add, get, remove or clear");
} 
